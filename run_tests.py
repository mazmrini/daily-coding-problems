import os
import re
import argparse
from typing import Union


parser = argparse.ArgumentParser(description='Runs all')
parser.add_argument('day', help='The day you want to run')
args = parser.parse_args()


def day_exists(day: int) -> bool:
    day_dir = f"day-{day}"

    return day_dir in os.listdir('.')


def run_test_for_day(day: Union[int, str]) -> None:
    day_dir = f"day-{day}"
    print(f"\n### {day_dir} ###\n")
    os.system(f"cd {day_dir} && python -m unittest --verbose")


def run_all_tests() -> None:
    days = []
    for file_name in os.listdir("."):
        day_search = re.search('^day-(\d+)$', file_name)

        if day_search:
            days.append(int(day_search.group(1)))

    for day in sorted(days):
        run_test_for_day(day)


if day_exists(args.day):
    run_test_for_day(args.day)
else:
    run_all_tests()
