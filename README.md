# Daily coding problems

- From their [website](https://www.dailycodingproblem.com/)
- Difficulties (easy/medium/hard) are the ones given by the resource
- Problems are detailed in a text file for each day


### Day 1
##### Google (easy)
Sum of 2 elements in an array

### Day 2
##### Uber (hard)
Product of all elements but the one at index i

### Day 3
##### Google (medium)
Serialize and deserialize a binary tree

### Day 4
##### Stripe (hard)
First missing positive integer in array in linear time and constant space
```text
Current solution is linear in time and space
```

### Day 5
##### Jane Street (medium)
`cons(a, b)` constructs a pair. Write `car(cons(a, b))` that returns the first element and `cdr(cons(a, b))` that returns the last element

### Day 6
##### Google (hard)
Implement a XOR doubly linked list

### Day 7
##### Facebook (medium)
Given the mapping `a = 1, b = 2, ... z = 26`, and an encoded message, count the number of ways it can be decoded

### Day 8
##### Google (easy)
Count the number of unival subtrees

### Day 9
##### Airbnb (hard)
Largest sum of non-adjacent numbers in linear time and constant space

### Day 10
##### Apple (medium)
Implement a job scheduler which takes in a function f and an integer n, and calls f after n milliseconds

### Day 11
##### Twitter (medium)
Implement an autocomplete system

### Day 12
##### Amazon (hard)
There exists a staircase with N steps. Given N, write a function that returns the number of unique ways you can climb the staircase. The order of the steps matters

### Day 13
##### Amazon (hard)
Find the length of the longest substring that contains at most k distinct characters

### Day 14
##### Google (medium)
Estimate π to 3 decimal places using a Monte Carlo method.
```text
Did only 2 decimal place, it takes too long to do more than that
```

### Day 15
##### Facebook (medium)
Pick a random element from a stream too large to store in memory

### Day 16
##### Twitter (easy)
Implement a data structure that logs the last 10 order ids

### Day 17
##### Google (hard)
Length of longest file path

### Day 18
##### Google (hard)
Compute the maximum values of each subarray of length in O(n) time and O(k) space
```text
Current solution is O(nk) in time and O(k) in space
```

### Day 19
##### Facebook (medium)
Given an N by K matrix where the nth row and kth column represents the cost to build the nth house with kth color, return the minimum cost

### Day 20
##### Google (easy)
Find the intersect of two linked lists. Do this in O(M + N)

### Day 21
##### Snapshat (easy)
Given an array of time intervals (start, end) for classroom lectures (possibly overlapping), find the minimum number of rooms required

### Day 22
##### Microsoft (medium)
Given a dictionary of words and a string made up of those words (no spaces), return the original sentence in a list. 
