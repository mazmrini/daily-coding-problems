import unittest
from solution import solution


class Tests(unittest.TestCase):
    def test_given_the_quick_brown_fox_then_returns_the_expected_list(self):
        words = {'quick', 'brown', 'the', 'fox'}
        sentence = 'thequickbrownfox'
        expected = ['the', 'quick', 'brown', 'fox']

        result = solution(sentence, words)

        self.assertEqual(expected, result)

    def test_given_bed_bath_and_beyond_then_returns_the_expected_list(self):
        words = {'and', 'beyond', 'bed', 'bath', 'bedbath'}
        sentence = 'bedbathandbeyond'
        expected = ['bedbath', 'and', 'beyond']
        expected_2 = ['bed', 'bath', 'and', 'beyond']

        result = solution(sentence, words)

        try:
            self.assertListEqual(expected, result)
        except:
            self.assertListEqual(expected_2, result)

    def test_given_no_possible_reconstruction_then_returns_none(self):
        sentence = 'goodbyemyfriend'

        result = solution(sentence, set())

        self.assertIsNone(result)

    def test_given_partial_reconstruction_then_returns_none(self):
        words = {'friend', 'goodbye'}
        sentence = 'goodbyemyfriend'

        result = solution(sentence, words)

        self.assertIsNone(result)

    def test_given_empty_sentence_then_returns_empty_list(self):
        words = {'and'}
        expected = []

        result = solution('', words)

        self.assertListEqual(result, expected)
