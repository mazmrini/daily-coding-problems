from typing import List, Union


def solution(sentence: str, words: set) -> Union[None, List[str]]:
    if len(sentence) == 0:
        return []

    result = []
    current_word = ''
    for letter in sentence:
        current_word += letter
        if current_word in words:
            result.append(current_word)
            current_word = ''

    return result if len(current_word) == 0 else None
