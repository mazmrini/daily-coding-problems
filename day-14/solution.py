import random
import threading
from typing import List, Union


def __estimate_pi(k, result, index) -> None:
    r2 = 0.5**2
    nb_in_circle = 0
    for _ in range(k):
        x = random.uniform(-0.5, 0.5)
        y = random.uniform(-0.5, 0.5)
        if x**2 + y**2 <= r2:
            nb_in_circle += 1

    result[index] = 4 * nb_in_circle / k


def solution(k: int=1000000, nb_threads: int=5) -> float:
    """
    circle of 0.5 of r in a square of length 1

    area of square = 1
    area of circle = pi * 0.5**2 = pi/4

    A.square / A.circle = pi/4
    4*A.square / A.circle = pi

    1. generate k points within [-0.5, 0.5] in x and y
    2. a point is within the circle when x2 + y2 <= r2
       else it is outside of it, but in the square
    3. count the (c)ircle/k points inside the circle
    4. the ratio 4.s/c approximately equals pi
    """

    threads: List[Union[None, threading.Thread]] = [None for _ in range(nb_threads)]
    results: List[Union[None, float]] = [None for _ in range(nb_threads)]

    for i in range(nb_threads):
        threads[i] = threading.Thread(target=__estimate_pi, args=(k, results, i))
        threads[i].start()

    for i in range(nb_threads):
        threads[i].join()

    return sum(results) / nb_threads
