import unittest
import math
from solution import solution


class Tests(unittest.TestCase):
    def test_solution_is_precise_at_2_decimal_places_of_pi(self):
        result = solution(k=250000, nb_threads=10)

        self.assertAlmostEqual(result, math.pi, places=2)
