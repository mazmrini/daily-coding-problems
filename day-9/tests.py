import unittest
from solution import solution


class Tests(unittest.TestCase):
    def test_given_empty_list_then_returns_0(self):
        expected = 0

        result = solution([])

        self.assertEqual(expected, result)

    def test_given_list_of_one_element_then_returns_the_element(self):
        element = 10

        result = solution([element])

        self.assertEqual(element, result)

    def test_given_list_with_two_elements_then_returns_the_biggest_one(self):
        big = 100
        small = 1

        result = solution([small, big])

        self.assertEqual(big, result)

    def test_given_list_with_three_elements_then_returns_the_expected_value(self):
        data = [6, 30, 2]
        expected = 30

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_the_provided_data_then_returns_13(self):
        data = [2, 4, 6, 2, 5]
        # 2 + 6 + 5
        expected = 13

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_the_provided_data_then_returns_10(self):
        data = [5, 1, 1, 5]
        # 5 + 5
        expected = 10

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_the_provided_data_then_returns_94(self):
        data = [2, 10, 10, 4, 8, 5, 8, 27, 13, 18, 11, 29, 2]
        # 2 + 10 + 8 + 27 + 18 + 29
        expected = 94

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_the_provided_data_then_returns_112(self):
        data = [2, 10, 10, 4, 8, 5, 8, 27, 55, 18, 11, 29, 2]
        # 2 + 10 + 8 + 8 + 55 + 29
        expected = 112

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_the_provided_with_negative_numbers_in_it_data_then_returns_165(self):
        data = [2, 10, -6, 0, -30, 10, 4, 8, -10, 5, 8, -14, -10, -3, 27, -1, 55, 0, 18, 11, 0, 29, 2]
        # 10 + 10 + 8 + 8 + 27 + 55 + 18 + 29
        expected = 165

        result = solution(data)

        self.assertEqual(expected, result)
