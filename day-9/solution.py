from typing import List


def solution(data: List[int]) -> int:
    total = 0
    i = -2
    while i < len(data):
        first_next_choice = data[i + 2] if i + 2 < len(data) else 0
        second_next_choice = data[i + 3] if i + 3 < len(data) else 0
        third_next_choice = data[i + 4] if i + 4 < len(data) else 0

        if first_next_choice + third_next_choice > second_next_choice:
            if first_next_choice > 0:
                total += first_next_choice

            # first choice
            i += 2
        else:
            if second_next_choice > 0:
                total += second_next_choice

            # second choice
            i += 3

    return total
