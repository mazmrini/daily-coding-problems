from typing import List, Union


def __min_positive_value(numbers: List[int]) -> Union[int, None]:
    min_positive_value = max(numbers)

    for number in numbers:
        if 0 <= number < min_positive_value:
            min_positive_value = number

    return min_positive_value if min_positive_value >= 0 else None


# linear in time, linear in space (looking for constant in space)
def solution(numbers: List[int]) -> int:
    if len(numbers) == 0:
        return 0

    max_value = max(numbers)
    if max_value < 0:
        return 0

    min_value = __min_positive_value(numbers)
    if min_value is None:
        return 0

    numbers = set(numbers)

    for number in range(min_value + 1, max_value + 1):
        if number not in numbers:
            return number

    return max_value + 1

