import unittest
import random
from typing import List

from solution import solution


class Tests(unittest.TestCase):
    lower_value = -10
    upper_value = 100
    max_nb_values_per_array = 70
    nb_arrays = 1000

    def test_given_array1_then_returns_expected_solution(self):
        data = [3, 4, -1, 1]
        expected = 2

        naive = self.__naive_solution(data)
        result = solution(data)

        self.assertEqual(expected, naive)
        self.assertEqual(expected, result)

    def test_given_array2_then_returns_expected_solution(self):
        data = [0, 2, 1]
        expected = 3

        naive = self.__naive_solution(data)
        result = solution(data)

        self.assertEqual(naive , expected, naive)
        self.assertEqual(expected, result)

    def test_given_array3_then_returns_expected_solution(self):
        data = [-5, 4, 10, 3, -1, 8, 2, 6]
        expected = 5

        naive = self.__naive_solution(data)
        result = solution(data)

        self.assertEqual(naive, expected, naive)
        self.assertEqual(expected, result)

    def test_given_array4_with_duplicates_then_returns_expected_solution(self):
        data = [-5, 1, 1, 1, 2, 4, 5, 9]
        expected = 3

        naive = self.__naive_solution(data)
        result = solution(data)

        self.assertEqual(naive, expected, naive)
        self.assertEqual(expected, result)

    def test_given_empty_array_then_returns_zero(self):
        data = []
        expected = 0

        naive = self.__naive_solution(data)
        result = solution(data)

        self.assertEqual(naive, expected, naive)
        self.assertEqual(expected, result)

    def test_given_array_with_negative_values_only_then_returns_zero(self):
        data = [-1, -3, -10, -50, -7]
        expected = 0

        naive = self.__naive_solution(data)
        result = solution(data)

        self.assertEqual(naive, expected, naive)
        self.assertEqual(expected, result)

    def test_given_random_arrays_then_expect_solution_to_match_the_naive_solution(self):
        for _ in range(self.nb_arrays):
            self.__given_random_arrays_then_expect_solution_to_match_the_naive_solution()

    def __given_random_arrays_then_expect_solution_to_match_the_naive_solution(self) -> None:
        nb_values_in_array = random.randint(0, self.max_nb_values_per_array)
        data = [random.randint(self.lower_value, self.upper_value) for _ in range(nb_values_in_array)]
        naive_solution = self.__naive_solution(data)

        result = solution(data)

        self.assertEqual(naive_solution, result, f"{data}")

    def __naive_solution(self, data: List[int]) -> int:
        if len(data) == 0:
            return 0

        data = set(filter(lambda x: x >= 0, data))
        data = sorted(data)
        if len(data) == 0:
            return 0

        result = data[0] + 1
        for value in data[1:]:
            if value == result:
                result += 1
            else:
                break

        return result
