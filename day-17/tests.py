import unittest
from solution import solution


'''
dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext\n\t\tsubsubdir3
dir
    subdir1
    subdir2
        file.ext
        subsubdir3
        
len(dir/subdir2/file.ext) = 20
###########
dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext
dir
    subdir1
        file1.ext
        subsubdir1
    subdir2
        subsubdir2
            file2.ext
            
len(dir/subdir2/subsubdir2/file2.ext) = 32
###############
dir\n\tsubdir1\n\tsubdir2\n\t\tsubdir3

dir
    subdir1
    subdir2
        subdir3
        
no files => 0
'''


class Tests(unittest.TestCase):
    def setUp(self):
        self.only_dir_in_file_system = "dir\n\tsubdir1\n\tsubdir2\n\t\tsubdir3"
        self.string_with_20_as_expected = "dir\n\tsubdir1\n\tsubdir2\n\t\tfile.ext\n\t\tsubsubdir3"
        self.string_with_32_as_expected = "dir\n\tsubdir1\n\t\tfile1.ext\n\t\tsubsubdir1\n\tsubdir2\n\t\tsubsubdir2\n\t\t\tfile2.ext"

    def test_given_empty_file_system_then_returns_0(self):
        expected = 0

        result = solution("")

        self.assertEqual(expected, result)

    def test_given_only_dir_in_file_system_then_returns_0(self):
        expected = 0

        result = solution(self.only_dir_in_file_system)

        self.assertEqual(expected, result)

    def test_given_string_with_20_as_expected_then_returns_20(self):
        expected = 20

        result = solution(self.string_with_20_as_expected)

        self.assertEqual(expected, result)

    def test_given_string_with_32_as_expected_then_returns_32(self):
        expected = 32

        result = solution(self.string_with_32_as_expected)

        self.assertEqual(expected, result)
