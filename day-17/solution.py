class LinkedListNode:
    def __init__(self) -> None:
        self.value = None
        self.next: LinkedListNode = None


class PathNode:
    def __init__(self, name: str, depth: int) -> None:
        self.name = name
        self.depth = depth
        self.subnodes = []

    def add_subnode(self, subnode: 'PathNode') -> None:
        self.subnodes.append(subnode)

    def is_file(self) -> bool:
        return "." in self.name

    def largest_file_path(self)-> str:
        largest_file_path = ""

        for subnode in self.subnodes:
            subnode_largest_file_path = subnode.largest_file_path()
            file_path = f"{self.name}/{subnode_largest_file_path}"

            if file_path > largest_file_path:
                largest_file_path = file_path

        if len(self.subnodes) > 0:
            return largest_file_path if "." in largest_file_path else "/"
        else:
            return self.name if self.is_file() else ""

    def __repr__(self) -> str:
        result = f"{' '*self.depth}{self.name}\n"
        for subnode in self.subnodes:
            result += f"{str(subnode)}"

        return f"{result}"


def __make_path_node(name: str) -> PathNode:
    depth = len(name) - len(name.strip())

    return PathNode(name.strip(), depth)


def __solution(root: PathNode, linked_list_node: LinkedListNode) -> None:
    if root.is_file():
        return

    list_current_node = linked_list_node
    while list_current_node.next is not None:
        current_path_node = __make_path_node(list_current_node.next.value)
        if current_path_node.depth == root.depth + 1:
            root.add_subnode(current_path_node)
            __solution(current_path_node, list_current_node.next)
            list_current_node.next = list_current_node.next.next
        else:
            break


def solution(text: str) -> int:
    if len(text) == 0:
        return 0

    root = PathNode("", -1)
    linked_list_head = LinkedListNode()
    current_node = linked_list_head
    for name in text.split("\n"):
        new_node = LinkedListNode()
        new_node.value = name
        current_node.next = new_node
        current_node = new_node

    __solution(root, linked_list_head)

    # -1 because it starts with /
    return len(root.largest_file_path()) - 1
