from typing import List, Tuple


class Class:
    def __init__(self, lecture: Tuple[int, int]) -> None:
        self.start = lecture[0]
        self.end = lecture[1]

    def add(self, lecture: Tuple[int, int]) -> 'Class':
        if self.start <= lecture[0] <= self.end or \
                self.start <= lecture[1] <= self.end:
            raise AttributeError

        self.end = lecture[1]

        return self


def solution(lectures: List[Tuple[int, int]]) -> int:
    if len(lectures) == 0:
        return 0

    lectures = sorted(lectures, key=lambda element: element[0])

    classrooms = []
    for lecture in lectures:
        for classroom in classrooms:
            try:
                classroom.add(lecture)
                break
            except AttributeError:
                pass
        else:
            classrooms.append(Class(lecture))

    return len(classrooms)
