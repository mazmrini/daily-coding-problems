import unittest
from solution import solution


class Tests(unittest.TestCase):
    def test_given_2_overlapping_lectures_then_returns_2(self):
        lectures = [(30, 75), (0, 50), (60, 150)]
        expected = 2

        result = solution(lectures)

        self.assertEqual(expected, result)

    def test_given_5_overlapping_lectures_then_returns_5(self):
        lectures = [(30, 75) for _ in range(5)]
        expected = 5

        result = solution(lectures)

        self.assertEqual(expected, result)

    def test_given_no_overlapping_lectures_then_returns_1(self):
        lectures = [(5, 10), (20, 25), (26, 30), (31, 50)]
        expected = 1

        result = solution(lectures)

        self.assertEqual(expected, result)

    def test_given_no_lectures_then_returns_0(self):
        expected = 0

        result = solution([])

        self.assertEqual(expected, result)
