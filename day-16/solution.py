class Node:
    def __init__(self, value: str) -> None:
        self.value = value
        self.prev = None
        self.next = None


class Log:
    def __init__(self, max_size: int=10) -> None:
        self.head = Node("")
        self.tail = Node("")

        self.head.next = self.tail
        self.tail.prev = self.head

        self.max_size = max_size
        self.current_size = 0

    def record(self, order_id: str) -> None:
        record_node = Node(order_id)
        record_node.prev = self.head
        record_node.next = self.head.next

        self.head.next.prev = record_node
        self.head.next = record_node
        self.current_size += 1

        if self.current_size == self.max_size + 1:
            self.current_size = self.max_size

            self.tail.prev.prev.next = self.tail
            self.tail.prev = self.tail.prev.prev

    def get_last(self, i: int) -> str:
        current_i = 0
        current_node = self.head.next
        while current_i < i:
            current_node = current_node.next
            current_i += 1

        return current_node.value
