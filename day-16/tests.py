import unittest
from solution import Log


class Tests(unittest.TestCase):
    LOG_MOCK = "LOG_MOCK"
    REAL_LOG = "REAL_LOG"

    def setUp(self):
        self.log = Log()

    def test_given_no_log_when_adding_logs_mocks_and_real_log_at_index_2_then_returns_real_log(self):
        for i in range(self.log.max_size):
            self.log.record(f"[{i}] {self.LOG_MOCK}")

        self.log.record(self.REAL_LOG)
        self.log.record(self.LOG_MOCK)
        self.log.record(self.LOG_MOCK)

        result = self.log.get_last(2)

        self.assertEqual(self.REAL_LOG, result)

    def test_given_no_log_when_adding_real_log_followed_by_7_log_mocks_then_real_log_is_at_index_7(self):
        self.log.record(self.REAL_LOG)
        for _ in range(7):
            self.log.record(self.LOG_MOCK)

        result = self.log.get_last(7)

        self.assertEqual(self.REAL_LOG, result)
