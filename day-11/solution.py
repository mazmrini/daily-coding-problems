from typing import Set


def solution(possible_queries: Set[str], query: str) -> Set[str]:
    filter_func = lambda possible_query: possible_query.lower().startswith(query.lower())

    return set(filter(filter_func, possible_queries))
