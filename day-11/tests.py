import unittest
from solution import solution


class Tests(unittest.TestCase):
    def setUp(self):
        self.possible_queries = {"dog", "deal", "deer"}

    def test_given_empty_possible_queries_when_querying_then_returns_empty_set(self):
        data = set()
        query = "whatever"
        expected = set()

        result = solution(data, query)

        self.assertSetEqual(expected, result)

    def test_given_possible_queries_when_querying_an_empty_string_then_returns_all_possible_queries(self):
        query = ""
        expected = self.possible_queries

        result = solution(self.possible_queries, query)

        self.assertSetEqual(expected, result)

    def test_given_possible_queries_then_returns_expected_values(self):
        query = "de"
        expected = {"deal", "deer"}

        result = solution(self.possible_queries, query)

        self.assertSetEqual(expected, result)
