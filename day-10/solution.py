from threading import Timer
from typing import Callable


def schedule(f: Callable[[None], None], n: int) -> None:
    timer = Timer(n/1000, f)
    timer.start()
