import unittest
from solution import solution_with_division, solution_without_division


class Tests(unittest.TestCase):
    def test_given_empty_list_when_solution_with_division_then_returns_empty_list(self):
        result = solution_with_division([])

        self.assertEqual([], result)

    def test_given_empty_list_when_solution_without_division_then_returns_empty_list(self):
        result = solution_without_division([])

        self.assertEqual([], result)

    def test_given_list_with_one_element_when_solution_with_division_then_returns_empty_list(self):
        result = solution_with_division([1])

        self.assertEqual([], result)

    def test_given_list_with_one_element_when_solution_without_division_then_returns_empty_list(self):
        result = solution_without_division([1])

        self.assertEqual([], result)

    def test_given_a_valid_list_when_solution_with_division_then_returns_expected_result(self):
        data = [1, 2, 3, 4, 5]
        expected = [120, 60, 40, 30, 24]

        result = solution_with_division(data)

        self.assertEqual(expected, result)

    def test_given_a_valid_list_when_solution_without_division_then_returns_expected_result(self):
        data = [1, 2, 3, 4, 5]
        expected = [120, 60, 40, 30, 24]

        result = solution_without_division(data)

        self.assertEqual(expected, result)

    def test_given_another_valid_list_when_solution_with_division_then_returns_expected_result(self):
        data = [3, 2, 1]
        expected = [2, 3, 6]

        result = solution_with_division(data)

        self.assertEqual(expected, result)

    def test_given_another_valid_list_when_solution_without_division_then_returns_expected_result(self):
        data = [3, 2, 1]
        expected = [2, 3, 6]

        result = solution_without_division(data)

        self.assertEqual(expected, result)
