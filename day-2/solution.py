from functools import reduce


def solution_with_division(numbers: list) -> list:
    if len(numbers) < 2:
        return []

    total = reduce(lambda x, y: x * y, numbers)

    return [total / numbers[i] for i in range(len(numbers))]


def __reduce_without_element(numbers: set, not_wanted_element: int) -> int:
    numbers.remove(not_wanted_element)

    total = reduce(lambda x, y: x * y, numbers)

    numbers.add(not_wanted_element)

    return total


# probably suboptimal
def solution_without_division(numbers: list) -> list:
    if len(numbers) < 2:
        return []

    numbers_as_set = set(numbers)

    return [__reduce_without_element(numbers_as_set, number)
            for number in numbers]
