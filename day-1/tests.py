import unittest
from solution import solution


class Tests(unittest.TestCase):
    def test_given_empty_list_then_returns_false(self):
        result = solution([], 10)

        self.assertFalse(result)

    def test_given_list_with_a_single_element_then_returns_false(self):
        result = solution([3], 20)

        self.assertFalse(result)

    def test_given_list_with_elements_that_can_sum_up_to_k_then_returns_true(self):
        data = [10, 15, 3, 7]
        k = 17

        result = solution(data, k)

        self.assertTrue(result)

    def test_given_list_with_elements_that_can_sum_up_to_negative_k_then_returns_true(self):
        data = [10, -20, 3, 7]
        k = -10

        result = solution(data, k)

        self.assertTrue(result)
