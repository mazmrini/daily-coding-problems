def solution(numbers: list, k: int) -> bool:
    complements = set()

    for number in numbers:
        complement = k - number

        if complement in complements:
            return True
        else:
            complements.add(number)

    return False
