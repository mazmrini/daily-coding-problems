import random
import unittest
from mockito import unstub, when, ANY
from solution import solution, Stream


class Tests(unittest.TestCase):
    def tearDown(self):
        unstub()

    def test_given_stream_then_returns_expected_random_value(self):
        data = list(range(0, 100))
        random_index = 33
        expected = random_index - 1
        when(random).uniform(ANY(int), ANY(int)).thenReturn(random_index)
        stream = Stream(data, 100)

        result = solution(stream)

        self.assertEqual(expected, result)
