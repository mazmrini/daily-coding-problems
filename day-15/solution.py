from typing import List, Iterable
import random


class Stream:
    def __init__(self, data: List[int], length: int) -> None:
        self.data = data
        self.length = length

    def stream(self) -> Iterable:
        for element in self.data:
            yield element


def solution(stream: Stream) -> int:
    random_index = random.uniform(0, stream.length)

    i = -1
    for el in stream.stream():
        element = el
        i += 1
        if i == random_index - 1:
            break

    return element
