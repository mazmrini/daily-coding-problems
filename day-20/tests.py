import unittest
from solution import solution, LinkedList


class Tests(unittest.TestCase):
    def test_given_two_list_of_same_length_with_some_intersect_then_returns_intersect_value(self):
        first = LinkedList.from_list([3, 7, 8, 10])
        second = LinkedList.from_list([99, 1, 8, 10])
        expected = 8

        result = solution(first, second)

        self.assertEqual(expected, result)

    def test_given_two_list_of_different_length_with_some_intersect_then_returns_the_intersect_value(self):
        first = LinkedList.from_list([3, 7, 5, 10, 12, 3, 9, 14])
        second = LinkedList.from_list([99, 1, 8, 10])
        expected = 10

        result = solution(first, second)

        self.assertEqual(expected, result)

    def test_given_non_intersecting_lists_then_returns_None(self):
        first = LinkedList.from_list([1, 2, 3, 4, 5])
        second = LinkedList.from_list([6, 7, 8])

        result = solution(first, second)

        self.assertIsNone(result)

    def test_given_one_empty_list_then_returns_None(self):
        first = LinkedList.from_list(list(range(10)))

        result = solution(first, LinkedList())

        self.assertIsNone(result)

    def test_given_two_empty_lists_then_returns_None(self):
        result = solution(LinkedList(), LinkedList())

        self.assertIsNone(result)
