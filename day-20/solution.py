from typing import Generator, Union, List


class Node:
    def __init__(self) -> None:
        self.value = None
        self.next = None


class LinkedList:
    def __init__(self) -> None:
        self.head = Node()
        self.length = 0

    @classmethod
    def from_list(cls, data: List[int]) -> 'LinkedList':
        linked_list = LinkedList()
        for element in data:
            linked_list.add(element)

        return linked_list

    def add(self, value: int) -> None:
        tmp = self.head
        while tmp.next is not None:
            tmp = tmp.next

        new_node = Node()
        new_node.value = value

        tmp.next = new_node
        self.length += 1

    def stream(self) -> Generator:
        tmp = self.head
        while tmp.next is not None:
            tmp = tmp.next
            yield tmp.value


def solution(first_list: LinkedList, second_list: LinkedList) -> Union[int, None]:
    min_range = min(first_list.length, second_list.length)

    first_generator = first_list.stream()
    second_generator = second_list.stream()
    for _ in range(min_range):
        first_value = next(first_generator)
        second_value = next(second_generator)
        if first_value == second_value:
            return first_value

    return None
