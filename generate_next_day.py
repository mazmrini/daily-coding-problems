import os
import re
import argparse

parser = argparse.ArgumentParser(description='Sets up the next day problem')
parser.add_argument('company', help='The company name')
parser.add_argument('difficulty', choices={'easy', 'medium', 'hard'},
                    help='The problem difficulty')
args = parser.parse_args()

days = []
for file_name in os.listdir("."):
    day_search = re.search('^day-(\d+)$', file_name)

    if day_search:
        days.append(int(day_search.group(1)))

last_day = sorted(days)[-1]
next_day = last_day + 1

next_day_dir = f'day-{next_day}'
os.mkdir(next_day_dir)


problem_file = os.path.join(next_day_dir, 'problem.txt')
header = f"{args.company.title()} ({args.difficulty})"
with open(problem_file, 'w') as f:
    f.write(f"{header} ")

readme_file = "README.md"
with open(readme_file, 'a+') as f:
    f.write(f"\n### Day {next_day}\n")
    f.write(f"##### {header}\n")
    f.write("Some description...\n")

solution_file = os.path.join(next_day_dir, 'solution.py')
with open(solution_file, 'w') as f:
    f.write("def solution(a, b):\n")
    f.write("    pass\n")

test_file = os.path.join(next_day_dir, 'tests.py')
with open(test_file, 'w') as f:
    f.write("import unittest\n")
    f.write("from solution import solution\n\n\n")
    f.write("class Tests(unittest.TestCase):\n")
    f.write("    def test(self):\n")
    f.write("        pass\n")
