import unittest
from solution import solution


'''
1-1-1
11-1
1-11

################

1-1-1-1-1
1-11-1-1
1-11-11
1-1-11-1
1-1-1-11

11-1-1-1
11-11-1
11-1-11

###############

1-2-3-4-3-2-1
1-23-4-3-2-1
1-23-4-3-21
1-2-3-4-3-21

12-3-4-3-2-1
12-3-4-3-21
'''


class Tests(unittest.TestCase):
    def test_given_1_then_returns_1(self):
        data = "1"
        expected = 1

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_21_then_returns_2(self):
        data = "21"
        expected = 2

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_38_then_returns_2(self):
        data = "38"
        expected = 1

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_111_then_returns_3(self):
        data = "111"
        expected = 3

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_11111_then_returns_8(self):
        data = "11111"
        expected = 8

        result = solution(data)

        self.assertEqual(expected, result)

    def test_given_12344321_then_returns_8(self):
        data = "12344321"
        expected = 6

        result = solution(data)

        self.assertEqual(expected, result)
