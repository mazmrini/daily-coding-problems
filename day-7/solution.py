def solution(text: str) -> int:
    if len(text) == 0:
        return 0

    if len(text) == 1:
        return 1

    if len(text) == 2:
        if 1 <= int(text[:2]) <= 26:
            return 2
        else:
            return 1

    total = solution(text[1:])
    if 1 <= int(text[:2]) <= 26:
        total += solution(text[2:])

    return total
