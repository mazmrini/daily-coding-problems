import unittest
import random
from solution import XORLinkedList, Memory


class Tests(unittest.TestCase):
    max_elements = 10
    expected_elements = [random.randint(0, 100) for _ in range(max_elements)]

    def setUp(self):
        self.linkedList = XORLinkedList()

    def tearDown(self):
        Memory.clear()

    def test_given_empty_list_when_getting_element_then_raises_index_error(self):
        with self.assertRaises(IndexError) as e:
            dummy = self.linkedList[Tests.max_elements]

    def test_given_empty_list_when_adding_an_element_then_element_is_added(self):
        element = 22
        self.linkedList.add(element)

        result = self.linkedList[0]

        self.assertEqual(element, result.value)

    def test_given_list_with_elements_when_getting_index_outside_range_then_raises_index_error(self):
        self.__given_list_with_elements()
        with self.assertRaises(IndexError) as e:
            dummy = self.linkedList[Tests.max_elements * 10]

    def test_given_list_with_elements_when_getting_indexes_then_returns_expected_values(self):
        self.__given_list_with_elements()
        for i, expected in enumerate(self.expected_elements):
            result = self.linkedList[i]

            self.assertEqual(expected, result.value, f"Failed for index {i}")

    def __given_list_with_elements(self):
        for element in self.expected_elements:
            self.linkedList.add(element)
