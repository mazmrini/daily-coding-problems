from typing import Tuple


class Memory:
    memory = {}

    @staticmethod
    def add(obj: any) -> None:
        Memory.memory[id(obj)] = obj

    @staticmethod
    def clear() -> None:
        Memory.memory.clear()

    @staticmethod
    def get_pointer(obj: any) -> int:
        return id(obj)

    @staticmethod
    def dereference_pointer(pointer: int) -> any:
        return Memory.memory[pointer]


class Node:
    def __init__(self) -> None:
        self.value = None
        self.both = None


class XORLinkedList:
    def __init__(self) -> None:
        self.length = 0
        head = Node()
        Memory.add(head)
        self.head_ptr = Memory.get_pointer(head)

    def add(self, element: int) -> None:
        new_node = Node()
        new_node.value = element
        Memory.add(new_node)

        head = Memory.dereference_pointer(self.head_ptr)
        new_node_ptr = Memory.get_pointer(new_node)

        if head.both is None:
            # head.both contains only next ptr since `a XOR 0 = a`
            head.both = new_node_ptr
        else:
            self.__add(new_node_ptr)

        self.length += 1

    def __getitem__(self, index: int) -> None:
        if not (0 <= index < self.length):
            raise IndexError(f"index [{index}] is out of range ({self.length})")

        i = 0
        previous_node_ptr = self.head_ptr
        current_node = Memory.dereference_pointer(Memory.dereference_pointer(self.head_ptr).both)
        while i < index:
            previous_node_ptr, current_node = self.__next(previous_node_ptr, current_node)

            i += 1

        return current_node

    def __add(self, new_node_ptr: int) -> None:
        previous_node_ptr = self.head_ptr
        current_node = Memory.dereference_pointer(Memory.dereference_pointer(self.head_ptr).both)

        while current_node.both is not None:
            previous_node_ptr, current_node = self.__next(previous_node_ptr, current_node)

        current_node.both = previous_node_ptr ^ new_node_ptr

    def __next(self, previous_node_ptr: int, current_node: Node) -> Tuple[int, Node]:
        next_node_ptr = current_node.both ^ previous_node_ptr
        previous_node_ptr = Memory.get_pointer(current_node)
        current_node = Memory.dereference_pointer(next_node_ptr)

        return previous_node_ptr, current_node
