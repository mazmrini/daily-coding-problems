import unittest
from solution import solution


'''
abcba, 2:
bcb (3)
----------
abbcdccaef, 3:
bbcdcc (6)
----------
a, 1:
(1)
----------
abcabcabc, 3:
abcabcabc (9)
'''


class Tests(unittest.TestCase):
    def test_given_empty_string_then_returns_0(self):
        expected = 0

        result = solution("", 33)

        self.assertEqual(expected, result)

    def test_given_abcba_and_k_eq_2_then_returns_3(self):
        text = "abcba"
        k = 2
        expected = 3

        result = solution(text, k)

        self.assertEqual(expected, result)

    def test_given_abbcdccaef_and_k_eq_3_then_returns_6(self):
        text = "abbcdccaef"
        k = 3
        expected = 6

        result = solution(text, k)

        self.assertEqual(expected, result)

    def test_given_a_and_k_eq_1_then_returns_1(self):
        text = "a"
        k = 1
        expected = 1

        result = solution(text, k)

        self.assertEqual(expected, result)

    def test_given_abcabcabc_and_k_eq_3_then_returns_9(self):
        text = "abcabcabc"
        k = 3
        expected = 9

        result = solution(text, k)

        self.assertEqual(expected, result)
