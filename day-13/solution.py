def solution(text: str, k: int) -> int:
    max_substring_length = 0

    for i, first_char in enumerate(text):
        longest_substring_length = 1
        substring = {first_char}
        for char in text[i+1:]:
            substring.add(char)
            if len(substring) <= k:
                longest_substring_length += 1
            else:
                break

        max_substring_length = max(max_substring_length, longest_substring_length)

    return max_substring_length
