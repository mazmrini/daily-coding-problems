from typing import Callable, TypeVar, Tuple

T = TypeVar('T')
GenericPairCallback = Callable[[T, T], T]


def cons(a: T, b: T) -> Callable[[GenericPairCallback], T]:
    def pair(f: GenericPairCallback) -> T:
        return f(a, b)
    return pair


def __make_pair(a: T, b: T) -> Tuple[T, T]:
    return a, b


# first element
def car(pair: Callable[[GenericPairCallback], T]) -> T:
    return pair(__make_pair)[0]


# last elemnet
def cdr(pair: Callable[[GenericPairCallback], T]) -> T:
    return pair(__make_pair)[-1]


# first element
def lambda_car(pair: Callable[[GenericPairCallback], T]) -> T:
    return pair(lambda first, last: first)


# last element
def lambda_cdr(pair: Callable[[GenericPairCallback], T]) -> T:
    return pair(lambda first, last: last)
