import unittest
from solution import cons, car, cdr, lambda_car, lambda_cdr


class Tests(unittest.TestCase):
    def test_given_3_4_pair_when_car_then_returns_3(self):
        expected = 3

        result = car(cons(3, 4))

        self.assertEqual(expected, result)

    def test_given_3_4_pair_when_cdr_then_returns_4(self):
        expected = 4

        result = cdr(cons(3, 4))

        self.assertEqual(expected, result)

    def test_given_3_4_pair_when_lambda_car_then_returns_3(self):
        expected = 3

        result = lambda_car(cons(3, 4))

        self.assertEqual(expected, result)

    def test_given_3_4_pair_when_lambda_cdr_then_returns_4(self):
        expected = 4

        result = lambda_cdr(cons(3, 4))

        self.assertEqual(expected, result)
