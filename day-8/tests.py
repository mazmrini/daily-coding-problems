import unittest
from solution import solution, Node


'''
(5)
   0
  / \
 1   0
    / \
   1   0
  / \
 1   1

##############
(8)
     0
    / \
   0   0
  / \   \
 0   0   0
        / \
       0   0
'''


class Tests(unittest.TestCase):
    def setUp(self):
        self.five_unival_root = Node(0,
                                     Node(1, None, None),
                                     Node(0,
                                          Node(1,
                                               Node(1, None, None),
                                               Node(1, None, None)
                                               ),
                                          Node(0, None, None)
                                          )
                                     )

        self.eight_unival_root = Node(0,
                                      Node(0,
                                           Node(0, None, None),
                                           Node(0, None, None)
                                           ),
                                      Node(0,
                                           None,
                                           Node(0,
                                                Node(0, None, None),
                                                Node(0, None, None)
                                                )
                                           )
                                      )

    def test_given_single_node_as_root_then_returns_1(self):
        root = Node(0, None, None)
        expected = 1

        result = solution(root)

        self.assertEqual(expected, result)

    def test_given_three_different_nodes_then_returns_2(self):
        root = Node(0,
                    Node(1, None, None),
                    Node(2, None, None))
        expected = 2

        result = solution(root)

        self.assertEqual(expected, result)

    def test_given_five_unival_root_then_returns_5(self):
        expected = 5

        result = solution(self.five_unival_root)

        self.assertEqual(expected, result)

    def test_given_eight_unival_root_then_returns_8(self):
        expected = 8

        result = solution(self.eight_unival_root)

        self.assertEqual(expected, result)
