from typing import Union, Tuple


class Node:
    def __init__(self, value: int, left: Union[None, 'Node'], right: Union[None, 'Node']) -> None:
        self.value = value
        self.left = left
        self.right = right

    def have_subnodes_the_same_value(self) -> bool:
        left_value = self.left.value if self.left is not None else self.value
        right_value = self.right.value if self.right is not None else self.value

        return self.value == right_value == left_value


def solution(root: Node) -> int:
    return __solution(root)[0]


def __solution(node: Node) -> Tuple[int, bool]:
    if node is None:
        return 0, True

    if node.left is None and node.right is None:
        return 1, True

    left_count, is_left_unival = __solution(node.left)
    right_count, is_right_unival = __solution(node.right)

    total = left_count + right_count
    is_unival = is_left_unival and is_right_unival and node.have_subnodes_the_same_value()
    if is_unival:
        total += 1

    return total, is_unival
