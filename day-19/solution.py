from typing import Tuple
import math


def __min_without_index(data: list, index: int) -> Tuple[int, int]:
    arg_min = 0
    min_value = math.inf

    for i, value in enumerate(data):
        if i == index:
            continue

        if value < min_value:
            min_value = value
            arg_min = i

    return arg_min, min_value


def __solution_for_2_houses_without_index(first: list, second: list, index: int) -> Tuple[int, int]:
    current_total = math.inf
    first_choice_index = -1
    second_choice_index = -1

    for i in range(0, len(first)):
        if i == index:
            continue

        tmp_first_choice_value = first[i]
        tmp_second_choice_index, tmp_second_choice_value = __min_without_index(second, i)

        if tmp_first_choice_value + tmp_second_choice_value < current_total:
            current_total = tmp_first_choice_value + tmp_second_choice_value
            first_choice_index = i
            second_choice_index = tmp_second_choice_index

    return first_choice_index, second_choice_index


def solution(data: list) -> int:
    if len(data) == 0:
        return 0

    if len(data) == 1:
        return min(data[0])

    if len(data) == 2:
        first_index, second_index = __solution_for_2_houses_without_index(data[0], data[1], -1)

        return data[0][first_index] + data[1][second_index]

    total = 0
    previous_index = -1
    for i in range(len(data) - 2):
        current_houses = data[i]
        next_houses = data[i + 1]
        next_next_houses = data[i + 2]

        current_total = math.inf
        best_price = math.inf
        tmp_previous_index = previous_index
        for j, price in enumerate(current_houses):
            if j == previous_index:
                continue

            tmp_next_index, tmp_next_next_index = __solution_for_2_houses_without_index(next_houses, next_next_houses, j)

            tmp_current_total = price + next_houses[tmp_next_index] + next_next_houses[tmp_next_next_index]
            if tmp_current_total < current_total:
                tmp_previous_index = j
                current_total = tmp_current_total
                if i + 1 == len(data) - 2:
                    best_price = tmp_current_total
                else:
                    best_price = price

        previous_index = tmp_previous_index
        total += best_price

    return total
