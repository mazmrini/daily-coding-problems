import unittest
from solution import solution


'''
1  5  9  7                   (2) 5
2  8  6  8                   (0) 2
total = 7
#########
100  40   5   10   8         (2) 5
10   3    3   0    10        (1) 3
35   15   17  11   37        (3) 11
11   29   15  42   77        (0) 11
total = 30
#########
100  40  5   10   8          (4) 8
100  50  1   100  5          (2) 1
43   5   12  17   1          (1) 1
11   4   7   9    100        (2) 4
total = 14
'''


class Tests(unittest.TestCase):
    def setUp(self):
        self.two_rows = [
            [1, 5, 9, 7],
            [2, 8, 6, 8]
        ]

        self.data_with_total_30 = [
            [100, 40, 5, 10, 8],
            [10, 3, 3, 0, 10],
            [35, 15, 17, 11, 37],
            [11, 29, 15, 42, 77]
        ]

        self.data_with_total_14 = [
            [100, 40, 5, 10, 8],
            [100, 50, 1, 100, 5],
            [43, 5, 12, 17, 1],
            [11, 4, 7, 9, 100]
        ]

    def test_given_data_with_total_30_then_returns_30(self):
        expected = 30
        
        result = solution(self.data_with_total_30)
        
        self.assertEqual(expected, result)

    def test_given_data_with_total_14_then_returns_14(self):
        expected = 14

        result = solution(self.data_with_total_14)

        self.assertEqual(expected, result)

    def test_given_empty_data_then_returns_0(self):
        expected = 0

        result = solution([])

        self.assertEqual(expected, result)

    def test_given_single_row_then_returns_min_value(self):
        expected = 0

        result = solution([[10, 20, 0, 30, 50]])

        self.assertEqual(expected, result)

    def test_given_two_rows_then_returns_7(self):
        expected = 7

        result = solution(self.two_rows)

        self.assertEqual(expected, result)
