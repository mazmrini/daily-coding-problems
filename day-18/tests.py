import unittest
from solution import solution


'''
[10, 5, 2, 7, 8, 7] with k = 3
becomes 
[10, 7, 8, 8]
#####################
[13, 2, 9, 4, 2, 1, 2, 8, 5, 9] with k = 5
becomes
[13, 9, 9, 8, 8, 8, 9]
'''


class Tests(unittest.TestCase):
    def test_given_data_with_k_equals_3_then_returns_expected_list(self):
        data = [10, 5, 2, 7, 8, 7]
        k = 3
        expected = [10, 7, 8, 8]

        result = solution(data, k)

        self.assertListEqual(expected, result)

    def test_given_data_with_k_equals_5_then_returns_expected_list(self):
        data = [13, 2, 9, 4, 2, 1, 2, 8, 5, 9]
        k = 5
        expected = [13, 9, 9, 8, 8, 9]

        result = solution(data, k)

        self.assertListEqual(expected, result)
