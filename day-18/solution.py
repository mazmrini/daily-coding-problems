from typing import List


def solution(data: List[int], k: int) -> List[int]:
    result = [-1 for _ in range(len(data) + 1 - k)]
    for i in range(len(data) + 1 - k):
        result[i] = max(data[i:i+k])

    return result
