import unittest
from solution import Node, Solution


'''
The full tree
    1
   / \
  2   9
 / \   \
3   4  10
   /   /
  5   11
 / \
6   7
     \
      8
---              
To the right tree
1
 \
  2
   \
    3
   / \
  4   6
 /     \
5       7
'''


class Tests(unittest.TestCase):
    def setUp(self):
        self.full_tree_serialized = '1<2<3<>>4<5<6<>>7<>8<>>>9<>10<11<>>'
        self.full_tree = Node(1,
                              Node(2,
                                   Node(3),
                                   Node(4,
                                        Node(5,
                                             Node(6),
                                             Node(7,
                                                  right=Node(8)
                                                  )
                                             )
                                        )
                                   ),
                              Node(9,
                                   right=Node(10,
                                              Node(11)
                                              )
                                   )
                              )

        self.to_the_right_tree_serialized = '1<>2<>3<4<5<>>>6<>7<>'
        self.to_the_right_tree = Node(1,
                                      right=Node(2,
                                                 right=Node(3,
                                                            Node(4,
                                                                 Node(5)
                                                                 ),
                                                            Node(6,
                                                                 right=Node(7)
                                                                 )
                                                            )
                                                 )
                                      )

        self.single_node_tree_serialized = '1<>'
        self.single_node_tree = Node(1)

        self.solution = Solution()

    def test_given_full_tree_when_serializing_then_returns_expected_output(self):
        result = self.solution.serialize(self.full_tree)

        self.assertEqual(self.full_tree_serialized, result)

    def test_given_serizalized_full_tree_when_deserializing_then_returns_the_full_tree(self):
        result = self.solution.deserialize(self.full_tree_serialized)

        self.assertEqual(self.full_tree, result)

    def test_given_full_tree_when_deserializing_the_serialized_tree_then_returns_the_full_tree(self):
        result = self.solution.deserialize(self.solution.serialize(self.full_tree))
        
        self.assertEqual(self.full_tree, result)

    def test_given_to_the_right_tree_when_serializing_then_returns_expected_output(self):
        result = self.solution.serialize(self.to_the_right_tree)

        self.assertEqual(self.to_the_right_tree_serialized, result)

    def test_given_serizalized_to_the_right_tree_when_deserializing_then_returns_the_to_the_right_tree(self):
        result = self.solution.deserialize(self.to_the_right_tree_serialized)

        self.assertEqual(self.to_the_right_tree, result)

    def test_given_to_the_right_tree_when_deserializing_the_serialized_tree_then_returns_the_to_the_right_tree(self):
        result = self.solution.deserialize(self.solution.serialize(self.to_the_right_tree))

        self.assertEqual(self.to_the_right_tree, result)
        
    def test_given_single_node_tree_when_serializing_then_returns_expected_output(self):
        result = self.solution.serialize(self.single_node_tree)

        self.assertEqual(self.single_node_tree_serialized, result)

    def test_given_serizalized_single_node_tree_when_deserializing_then_returns_the_single_node_tree(self):
        result = self.solution.deserialize(self.single_node_tree_serialized)

        self.assertEqual(self.single_node_tree, result)

    def test_given_single_node_tree_when_deserializing_the_serialized_tree_then_returns_the_single_node_tree(self):
        result = self.solution.deserialize(self.solution.serialize(self.single_node_tree))

        self.assertEqual(self.single_node_tree, result)
