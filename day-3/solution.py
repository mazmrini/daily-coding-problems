from typing import Tuple, Union


class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right

    def __eq__(self, other: 'Node') -> bool:
        return str(self.val) == str(other.val) and \
                self.left == other.left and \
                self.right == other.right


class Solution:
    left_char = '<'
    right_char = '>'

    def serialize(self, node: Node) -> str:
        result = str(node.val)

        result += Solution.left_char
        result += '' if node.left is None else self.serialize(node.left)

        result += Solution.right_char
        result += '' if node.right is None else self.serialize(node.right)

        return result

    def deserialize(self, tree: str) -> Union[Node, None]:
        return self.__deserialize(tree)[0]

    def __deserialize(self, tree: str) -> Tuple[Union[Node, None], str]:
        if len(tree) == 0:
            return None, ""

        if tree[0] == Solution.right_char:
            return None, tree[1:]

        first_left_node_index = tree.index(Solution.left_char)
        first_right_node_index = tree.index(Solution.right_char)

        left_node = None
        val = tree[:first_right_node_index]
        rest_of_tree = tree[first_right_node_index + 1:]
        if first_left_node_index < first_right_node_index:
            val = tree[:first_left_node_index]
            rest_of_tree = tree[first_left_node_index + 1:]
            left_node, rest_of_tree = self.__deserialize(rest_of_tree)

        right_node, rest_of_tree = self.__deserialize(rest_of_tree)

        return Node(val, left_node, right_node), rest_of_tree
