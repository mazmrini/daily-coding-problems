import unittest
from solution import solution


'''
with {1, 2}
-----
4:
1-1-1-1
1-1-2
1-2-1
2-1-1
2-2

###########

7:
1-1-1-1-1-1-1

1-2-1-1-1-1
1-2-2-1-1
1-2-2-2
1-2-1-2-1
1-2-1-1-2

1-1-2-1-1-1
1-1-2-2-1
1-1-2-1-2

1-1-1-2-1-1
1-1-1-2-2

1-1-1-1-2-1
1-1-1-1-1-2

2-1-1-1-1-1
2-2-1-1-1
2-2-1-2
2-2-2-1

2-1-2-1-1
2-1-2-2

2-1-1-2-1
2-1-1-1-2

---
with {1, 3, 5}
---
5:
1-1-1-1-1
1-3-1
1-1-3

3-1-1

5
###################
8:
1-1-1-1-1-1-1-1
1-3-1-1-1-1
1-3-3-1
1-3-1-3
1-1-3-1-1-1
1-1-3-3
1-1-1-3-1-1
1-1-1-1-3-1
1-1-1-1-1-3
1-5-1-1
1-1-5-1
1-1-1-5

3-1-1-1-1-1
3-3-1-1
3-1-3-1
3-1-1-3
3-5

5-1-1-1
5-3
'''


class Tests(unittest.TestCase):
    def setUp(self):
        self.one_two_set = {1, 2}
        self.one_three_five_set = {1, 3, 5}

    def test_given_1_stair_and_1_2_set_then_should_return_1(self):
        nb_stairs = 1
        expected = 1

        result = solution(nb_stairs, self.one_two_set)

        self.assertEqual(expected, result)

    def test_given_2_stairs_and_1_2_set_then_should_return_2(self):
        nb_stairs = 2
        expected = 2

        result = solution(nb_stairs, self.one_two_set)

        self.assertEqual(expected, result)

    def test_given_4_stairs_and_1_2_set_then_should_return_5(self):
        nb_stairs = 4
        expected = 5

        result = solution(nb_stairs, self.one_two_set)

        self.assertEqual(expected, result)

    def test_given_7_stairs_and_1_2_set_then_should_return_21(self):
        nb_stairs = 7
        expected = 21

        result = solution(nb_stairs, self.one_two_set)

        self.assertEqual(expected, result)

    def test_given_5_stairs_and_1_3_5_set_then_should_return_5(self):
        nb_stairs = 5
        expected = 5

        result = solution(nb_stairs, self.one_three_five_set)

        self.assertEqual(expected, result)

    def test_given_8_stairs_and_1_3_5_set_then_should_return_19(self):
        nb_stairs = 8
        expected = 19

        result = solution(nb_stairs, self.one_three_five_set)

        self.assertEqual(expected, result)
