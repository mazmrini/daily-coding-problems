def solution(n: int, climb_values: set) -> int:
    if n < 0:
        return 0

    if n == 0:
        return 1

    total = 0
    for climb_value in climb_values:
        rest = n - climb_value
        total += solution(rest, climb_values)

    return total
